package com.xht.cheques.ui.login;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.xht.cheque.utils.Login;
import com.xht.cheques.BuildConfig;
import com.xht.cheques.R;
import com.xht.cheques.ui.base.BaseActivity;
import com.xht.cheques.ui.login.bean.LoginBean;
import com.xht.cheques.ui.mine.MainActivity;
import com.xht.cheques.ui.mine.bean.UpdateVersionBean;
import com.xht.cheques.utils.DialogUtils;
import com.xht.cheques.utils.Utils;
import com.xht.cheques.widget.ClearEditText;
import com.xht.kuaiyouyi.api.KyyChequeConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.utils.ToastU;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class LoginActivity extends BaseActivity {
    @BindView(R.id.clearedittext_account)
    ClearEditText et_account;
    @BindView(R.id.clearedittext_pass)
    ClearEditText et_pass;
    @BindView(R.id.imageview_pass)
    ImageView iv_pass;
    @BindView(R.id.button_login)
    Button button_login;

    private boolean isshow=true;
    private long exitTime;
    @Override
    protected int getLayout() {
        return R.layout.login_activity;
    }

    @Override
    protected void initView() {

        if(Login.Companion.getInstance().isLogin()){
            goToActivity(MainActivity.class);
            finish();
        }
        et_account.addTextChangedListener(new MyTextWatcher());
        et_pass.addTextChangedListener(new MyTextWatcher());
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = et_account.getText().toString().trim();
                String password = et_pass.getText().toString().trim();
                login(username, password);
            }
        });

        iv_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isshow){
                    isshow=false;
                    et_pass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    iv_pass.setImageResource(R.mipmap.login_icon_eye_nor);
                }else {
                    isshow=true;
                    et_pass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    iv_pass.setImageResource(R.mipmap.login_icon_closedeyes_nor);
                }
            }
        });

    }

    private void login(String username,String password){
        NetUtil.Companion.getInstance().url(KyyChequeConstants.INSTANCE.getURL_LOGIN())
                .addParam("type","1")
                .addParam("username",username)
                .addParam("password",password)
                .addParam("client", Utils.isHUAWEI() ? "HUAWEI":"")
                .addParam("l_l_p_type",Utils.isHUAWEI() ? "1":"")
                .addParam("l_l_p_token",Login.Companion.getInstance().getHuawei_push_token())
                .withPOST(new NetCallBack<LoginBean>() {
                    @NotNull
                    @Override
                    public Class<LoginBean> getRealType() {
                        return LoginBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        ToastU.INSTANCE.showToast(err);
                    }

                    @Override
                    public void onSuccess(@NonNull LoginBean loginBean) {
                        if(!TextUtils.isEmpty(loginBean.getToken()) && !TextUtils.isEmpty(loginBean.getToken_id())){
                            Login.Companion.getInstance().setToken(loginBean.getToken());
                            Login.Companion.getInstance().setToken_id(loginBean.getToken_id());
                            if(TextUtils.isEmpty(getIntent().getStringExtra("flag"))){
                                goToActivity(MainActivity.class);
                            }
                            finish();
                        }

                    }
                },false);
    }

    class MyTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String username = et_account.getText().toString();
            String password = et_pass.getText().toString();
            if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
                button_login.setEnabled(true);
            } else {
                button_login.setEnabled(false);
            }
        }
    }

    public void exit() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }



}
