package com.xht.cheques.ui.mine.bean;

public class MineBean {

    /**
     * ticket_collector : {"name":"想想","mobile":"13400134000","serial_number":"1111","avatar":"http://127.0.0.1/data/upload/shop/common/05887001303095333.png"}
     */

    private TicketCollectorBean ticket_collector;

    public TicketCollectorBean getTicket_collector() {
        return ticket_collector;
    }

    public void setTicket_collector(TicketCollectorBean ticket_collector) {
        this.ticket_collector = ticket_collector;
    }

    public static class TicketCollectorBean {
        /**
         * name : 想想
         * mobile : 13400134000
         * serial_number : 1111
         * avatar : http://127.0.0.1/data/upload/shop/common/05887001303095333.png
         */

        private String name;
        private String mobile;
        private String serial_number;
        private String avatar;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getSerial_number() {
            return serial_number;
        }

        public void setSerial_number(String serial_number) {
            this.serial_number = serial_number;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }
}
