package com.xht.cheques.ui.mine.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xht.cheques.R;
import com.xht.cheques.ui.mine.bean.TaskListBean;
import com.xht.cheques.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskAdapter extends BaseAdapter {
    private Context mContext;
    private List<TaskListBean.TicketListBean> list;
    private String type;

    public TaskAdapter(Context mContext, List<TaskListBean.TicketListBean> list,String type) {
        this.mContext = mContext;
        this.list = list;
        this.type=type;
    }

    @Override
    public int getCount() {
        if(list!=null && list.size()!=0){
            return list.size();
        }else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHolder;
        if(convertView==null){
            convertView=View.inflate(mContext, R.layout.item_taskfragment_layout,null);
            mViewHolder=new ViewHolder(convertView);
            convertView.setTag(mViewHolder);
        }else {
            mViewHolder= (ViewHolder) convertView.getTag();
        }
        if(!TextUtils.isEmpty(list.get(position).getOrder_main_id())){
            mViewHolder.tv_orderid.setText(list.get(position).getOrder_main_sn());
        }
        if(!TextUtils.isEmpty(list.get(position).getAddress_check_true_name())){
            mViewHolder.tv_address.setText(list.get(position).getAddress_check_area_info()+list.get(position).getAddress_check_address());
        }
        if(!TextUtils.isEmpty(list.get(position).getActive_status())){
            if(list.get(position).getActive_status().equals("1")){
                mViewHolder.tv_context.setText(mContext.getResources().getString(R.string.tickets_context)
                        +mContext.getResources().getString(R.string.tickets_context_q));
            }else if(list.get(position).getActive_status().equals("2")){
                mViewHolder.tv_context.setText(mContext.getResources().getString(R.string.tickets_context)
                        +mContext.getResources().getString(R.string.tickets_context_f));
            }else if(list.get(position).getActive_status().equals("3")){
                mViewHolder.tv_context.setText(mContext.getResources().getString(R.string.tickets_context)
                        +mContext.getResources().getString(R.string.tickets_context_s));
            }
        }
        if(!TextUtils.isEmpty(list.get(position).getAddress_check_true_name())){
            mViewHolder.tv_contact.setVisibility(View.VISIBLE);
            mViewHolder.tv_contact.setText("联  系  人："
                    +list.get(position).getAddress_check_true_name());
        }else {
            mViewHolder.tv_contact.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(list.get(position).getAddress_check_phone())){
            mViewHolder.tv_phone.setVisibility(View.VISIBLE);
            if(TextUtils.isEmpty(list.get(position).getMobile_zone1())){
                mViewHolder.tv_phone.setText(mContext.getResources().getString(R.string.phone_number)
                        +list.get(position).getAddress_check_phone());
            }else {
                mViewHolder.tv_phone.setText(mContext.getResources().getString(R.string.phone_number)+"+"
                        +list.get(position).getMobile_zone1()+"-"+list.get(position).getAddress_check_phone());
            }
        }else {
            mViewHolder.tv_phone.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(list.get(position).getTel_phone())){
            mViewHolder.tv_fixed_phone.setVisibility(View.VISIBLE);
            if(TextUtils.isEmpty(list.get(position).getMobile_zone2())){
                if(TextUtils.isEmpty(list.get(position).getTel_zone())){
                    mViewHolder.tv_fixed_phone.setText(mContext.getResources().getString(R.string.fixed_telephone)+list.get(position).getTel_phone());
                }else {
                    mViewHolder.tv_fixed_phone.setText(mContext.getResources().getString(R.string.fixed_telephone)+"+"
                    +list.get(position).getTel_zone()+"-"+list.get(position).getTel_phone());
                }
            }else {
                if(TextUtils.isEmpty(list.get(position).getTel_zone())){
                    mViewHolder.tv_fixed_phone.setText(mContext.getResources().getString(R.string.fixed_telephone)+"+"
                            +list.get(position).getMobile_zone2()+"-"
                            +list.get(position).getTel_phone());
                }else {
                    mViewHolder.tv_fixed_phone.setText(mContext.getResources().getString(R.string.fixed_telephone)+"+"
                            +list.get(position).getMobile_zone2()+"-"
                            +list.get(position).getTel_zone()+"-"+list.get(position).getTel_phone());
                }
            }
        }else {
            mViewHolder.tv_fixed_phone.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(list.get(position).getAddress_check_phone_spare())){
            mViewHolder.tv_alternate_phone.setVisibility(View.VISIBLE);
            if(TextUtils.isEmpty(list.get(position).getMobile_zone1_spare())){
                mViewHolder.tv_alternate_phone.setText(mContext.getResources().getString(R.string.alternate_phone)
                        +list.get(position).getAddress_check_phone_spare());
            }else {
                mViewHolder.tv_alternate_phone.setText(mContext.getResources().getString(R.string.alternate_phone)+"+"
                        +list.get(position).getMobile_zone1_spare()+"-"
                        +list.get(position).getAddress_check_phone_spare());
            }

        }else {
            mViewHolder.tv_alternate_phone.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(list.get(position).getSend_man_time())){
            mViewHolder.tv_send_single_time.setVisibility(View.VISIBLE);
            mViewHolder.tv_send_single_time.setText(mContext.getResources().getString(R.string.send_single_time)
                    +Utils.getDisplayDataAndTime(list.get(position).getSend_man_time()));
        }else {
            mViewHolder.tv_send_single_time.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(list.get(position).getGet_ticket_time())){
            mViewHolder.tv_collect_tickets_time.setVisibility(View.VISIBLE);
            mViewHolder.tv_collect_tickets_time.setText(mContext.getResources().getString(R.string.collect_tickets_time)
                    + Utils.getDisplayDataAndTime(list.get(position).getGet_ticket_time()));
        }else {
            mViewHolder.tv_collect_tickets_time.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(list.get(position).getIn_ticket_time())){
            mViewHolder.tv_ticket_time.setVisibility(View.VISIBLE);
            mViewHolder.tv_ticket_time.setText(mContext.getResources().getString(R.string.ticket_time)
                    +Utils.getDisplayDataAndTime(list.get(position).getIn_ticket_time()));
        }else {
            mViewHolder.tv_ticket_time.setVisibility(View.GONE);
        }

        if(type.equals("1")){
            mViewHolder.set_ticket.setText(R.string.set_ticket);
        }else if(type.equals("2")){
            mViewHolder.set_ticket.setText(R.string.in_ticket);
        }else {
            mViewHolder.set_ticket.setText("");
        }
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.tv_orderid)
        TextView tv_orderid;
        @BindView(R.id.tv_address)
        TextView tv_address;
        @BindView(R.id.tv_contact)
        TextView tv_contact;
        @BindView(R.id.tv_phone)
        TextView tv_phone;
        @BindView(R.id.tv_send_single_time)
        TextView tv_send_single_time;
        @BindView(R.id.tv_collect_tickets_time)
        TextView tv_collect_tickets_time;
        @BindView(R.id.tv_ticket_time)
        TextView tv_ticket_time;
        @BindView(R.id.set_ticket)
        TextView set_ticket;
        @BindView(R.id.tv_fixed_phone)
        TextView tv_fixed_phone;
        @BindView(R.id.tv_context)
        TextView tv_context;
        @BindView(R.id.tv_alternate_phone)
        TextView tv_alternate_phone;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
