package com.xht.cheques.ui.mine;

import android.content.Intent;
import android.graphics.Bitmap;
import android.preference.PreferenceActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.lzy.imagepicker.ui.ImagePreviewDelActivity;
import com.xht.cheque.utils.Login;
import com.xht.cheque.utils.SystemUtil;
import com.xht.cheques.R;
import com.xht.cheques.ui.base.BaseActivity;
import com.xht.cheques.ui.mine.adapter.AddImageAdapter;
import com.xht.cheques.utils.DialogUtils;
import com.xht.cheques.utils.SignUtil;
import com.xht.cheques.utils.Utils;
import com.xht.cheques.widget.ButtomDialog;
import com.xht.kuaiyouyi.api.KyyChequeConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

public class DemoActivity extends BaseActivity implements AddImageAdapter.OnRecyclerViewItemClickListener{
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.editText)
    EditText editText;
    @BindView(R.id.tv_add_notes)
    TextView tv_add_notes;
    @BindView(R.id.tv_cancel)
    TextView tv_cancel;
    public static final int IMAGE_ITEM_ADD = -1;
    public static final int REQUEST_CODE_SELECT = 100;
    public static final int REQUEST_CODE_PREVIEW = 101;
    private ButtomDialog mButtomDialog;

    private AddImageAdapter adapter;
    private List<ImageItem> selImageList; //当前选择的所有图片
    private int maxImgCount = 6;               //允许选择图片最大数
    private ArrayList<ImageItem> images = null;
    private List<File> files;
    private int count=0;
    private String payinfo_id;
    private String type;
    private String remark_content=null;
    private String remark_image=null;
    private List<String> imglist;
    @Override
    protected int getLayout() {
        return R.layout.activity_upremark;
    }

    @Override
    protected void initView() {
        type=getIntent().getStringExtra("type");
        payinfo_id=getIntent().getStringExtra("payinfo_id");
        files=new ArrayList<>();
        selImageList=new ArrayList<>();
        imglist=new ArrayList<>();
        adapter = new AddImageAdapter(DemoActivity.this, selImageList, maxImgCount);
        adapter.setOnItemClickListener(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_add_notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestUploadIcon();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == ImagePicker.RESULT_CODE_ITEMS) {
            //添加图片返回
            if (data != null && requestCode == REQUEST_CODE_SELECT) {
                images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                if (images != null) {
                    selImageList.addAll(images);
                    adapter.setImages(selImageList);
                }
                for(int i=0;i<selImageList.size();i++){
                    float degrees=Utils.readPictureDegree(selImageList.get(i).path);
                    Bitmap bitmap= Utils.rotateToDegrees(Utils.getimage(selImageList.get(count).path),degrees);
                    File file=Utils.compressImage(bitmap,selImageList.get(i).path);
                    files.add(file);
                }
            }
        } else if (resultCode == ImagePicker.RESULT_CODE_BACK) {
            //预览图片返回
            if (data != null && requestCode == REQUEST_CODE_PREVIEW) {
                images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_ITEMS);
                if (images != null) {
                    selImageList.clear();
                    selImageList.addAll(images);
                    adapter.setImages(selImageList);
                }
                for(int i=0;i<selImageList.size();i++){
                    float degrees=Utils.readPictureDegree(selImageList.get(i).path);
                    Bitmap bitmap= Utils.rotateToDegrees(Utils.getimage(selImageList.get(count).path),degrees);
                    File file=Utils.compressImage(bitmap,selImageList.get(i).path);
                    files.add(file);
                }
            }
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        switch (position){
            case IMAGE_ITEM_ADD:
                showButtonDialog();
                break;
            default:
                //打开预览
                Intent intentPreview = new Intent(this, ImagePreviewDelActivity.class);
                intentPreview.putExtra(ImagePicker.EXTRA_IMAGE_ITEMS, (ArrayList<ImageItem>) adapter.getImages());
                intentPreview.putExtra(ImagePicker.EXTRA_SELECTED_IMAGE_POSITION, position);
                intentPreview.putExtra(ImagePicker.EXTRA_FROM_ITEMS, true);
                startActivityForResult(intentPreview, REQUEST_CODE_PREVIEW);
                break;


        }

    }

    private void showButtonDialog() {
        if(mButtomDialog == null){
            View view = LayoutInflater.from(this).inflate(R.layout.dialog_buttom_three_btn,null);
            mButtomDialog = new ButtomDialog(this,view,false,true);
            TextView tv_three = view.findViewById(R.id.tv_three);
            TextView tv_one = view.findViewById(R.id.tv_one);
            TextView tv_two = view.findViewById(R.id.tv_two);

            tv_three.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                }
            });
            tv_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImagePicker.getInstance().setSelectLimit(maxImgCount - selImageList.size());
                    Intent intent = new Intent(DemoActivity.this, ImageGridActivity.class);
                    intent.putExtra(ImageGridActivity.EXTRAS_TAKE_PICKERS, true); // 是否是直接打开相机
                    startActivityForResult(intent, REQUEST_CODE_SELECT);
                    mButtomDialog.dismiss();
                }
            });
            tv_two.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //打开选择,本次允许选择的数量
                    ImagePicker.getInstance().setSelectLimit(maxImgCount - selImageList.size());
                    Intent intent = new Intent(DemoActivity.this, ImageGridActivity.class);
//                    /* 如果需要进入选择的时候显示已经选中的图片，
//                     * 详情请查看ImagePickerActivity
//                     * */
//                    intent.putExtra(ImageGridActivity.EXTRAS_IMAGES,images);
                    startActivityForResult(intent, REQUEST_CODE_SELECT);
                    mButtomDialog.dismiss();
                }
            });
        }
        mButtomDialog.show();
    }

    //上传图片
    private void requestUploadIcon() {
        if(count<selImageList.size()){
            String t = SystemUtil.INSTANCE.getTime(SystemUtil.INSTANCE.
                    getTimeStr("" + System.currentTimeMillis()));
            Map<String,Object> map = new HashMap<>();
            map.put("device_uuid", NetUtil.Companion.getUuid());
            map.put("device_name", NetUtil.Companion.getDevice_name());
            NetUtil.Companion.getInstance().url(KyyChequeConstants.INSTANCE.getURL_UPLODE_IMGMORE())
                    .addParam("device_uuid", NetUtil.Companion.getUuid())
                    .addParam("device_name", NetUtil.Companion.getDevice_name())
                    .addParam("token_id", Login.Companion.getInstance().getToken_id())
                    .addParam("sign_time", t)
                    .addParam("sign", SignUtil.sign(map, t))
                    .addParam(files.get(0).getName(),files.get(0))
                    .addParam(files.get(1).getName(),files.get(1))
                    .withPOSTFiles(new NetCallBack<String>() {
                        @NotNull
                        @Override
                        public Class<String> getRealType() {
                            return String.class;
                        }

                        @Override
                        public void onSuccess(@NonNull String uploadImageBean) {
                            Log.i("info","-------uploadImageBean"+uploadImageBean);
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {

                        }
                    },false,files);
        }
    }

}
