package com.xht.cheques.ui.mine.bean;

public class MessageEvent {
    private String message;

    public static final String TASK_NUM = "task_num";
    public static final String TASK_LIST="task_list";
    public static final String CHECK_INFO="check_num";
    public static final String MAIN="main";

    public MessageEvent(String message){
        this.message=message;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}


