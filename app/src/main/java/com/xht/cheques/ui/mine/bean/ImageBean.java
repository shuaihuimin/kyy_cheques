package com.xht.cheques.ui.mine.bean;

import java.util.List;

public class ImageBean {


    /**
     * file_name : {"upload_file":["company_remark_25bf3ca559fd38.png20181120164819","company_remark_25bf3ca55a0cbf.png20181120164819"]}
     */

    private FileNameBean file_name;

    public FileNameBean getFile_name() {
        return file_name;
    }

    public void setFile_name(FileNameBean file_name) {
        this.file_name = file_name;
    }

    public static class FileNameBean {
        private List<String> upload_file;

        public List<String> getUpload_file() {
            return upload_file;
        }

        public void setUpload_file(List<String> upload_file) {
            this.upload_file = upload_file;
        }
    }
}
