package com.xht.cheques.ui.mine.bean;

public class TasknumBean {

    /**
     * count_get : 0
     * count_in : 1
     * count_already_in : 0
     */

    private String count_get;
    private String count_in;
    private String count_already_in;

    public String getCount_get() {
        return count_get;
    }

    public void setCount_get(String count_get) {
        this.count_get = count_get;
    }

    public String getCount_in() {
        return count_in;
    }

    public void setCount_in(String count_in) {
        this.count_in = count_in;
    }

    public String getCount_already_in() {
        return count_already_in;
    }

    public void setCount_already_in(String count_already_in) {
        this.count_already_in = count_already_in;
    }
}
