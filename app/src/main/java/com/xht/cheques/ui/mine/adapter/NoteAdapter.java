package com.xht.cheques.ui.mine.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xht.cheques.R;
import com.xht.cheques.ui.mine.ImagesDisplayActivity;
import com.xht.cheques.ui.mine.bean.CheckInfoBean;
import com.xht.cheques.ui.mine.bean.MessageEvent;
import com.xht.cheques.utils.DialogUtils;
import com.xht.cheques.utils.Utils;
import com.xht.kuaiyouyi.api.KyyChequeConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoteAdapter extends BaseAdapter {
    private Context mContext;
    private List<CheckInfoBean.TicketInfoBean.RemarkArrBean> list;
    private String mtype;
    private String imgs[]=null;

    public NoteAdapter(Context mContext, List<CheckInfoBean.TicketInfoBean.RemarkArrBean> list,String type) {
        this.mContext = mContext;
        this.list = list;
        this.mtype=type;
    }

    @Override
    public int getCount() {
        if(list!=null && list.size()!=0){
            return list.size();
        }else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHolder;
        if(convertView==null){
            convertView=View.inflate(mContext, R.layout.item_notelist_layout,null);
            mViewHolder=new ViewHolder(convertView);
            convertView.setTag(mViewHolder);
        }else {
            mViewHolder= (ViewHolder) convertView.getTag();
        }
        Log.i("info","------------list.size="+list.get(position).getRemark_image_group().size());
        if(list.get(position).getRemark_image_group()!=null && list.get(position).getRemark_image_group().size()!=0){
            mViewHolder.gridlayout.setVisibility(View.VISIBLE);
            if(mViewHolder.gridlayout!=null){
                mViewHolder.gridlayout.removeAllViews();
            }
            int rowCount = 0;
            int hang;
            final int lie;
            if(list.get(position).getRemark_image_group().size()<=3){
                hang=1;
                lie=list.get(position).getRemark_image_group().size();
            }else {
                hang=2;
                lie=3;
            }
            for (int n = 0; n < hang; n++) { // 控制行
                for(int m=0;m<lie;m++){// 控制列
                    GridLayout.Spec rowspec=GridLayout.spec(n);
                    GridLayout.Spec columnspec=GridLayout.spec(m);
                    GridLayout.LayoutParams layoutParams=new GridLayout.LayoutParams(rowspec,columnspec);
                    View view=View.inflate(mContext,R.layout.item_gridlayot,null);
                    ImageView imageView = view.findViewById(R.id.iv_gridview);
                    if(rowCount<list.get(position).getRemark_image_group().size()){
                        Glide.with(mContext).load(list.get(position).getRemark_image_group().get(rowCount)).into(imageView);
                        mViewHolder.gridlayout.addView(view,layoutParams);
                        final int pos=rowCount;
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                imgs=new String[list.get(position).getRemark_image_group().size()];
                                for(int i=0;i<list.get(position).getRemark_image_group().size();i++){
                                    imgs[i]=list.get(position).getRemark_image_group().get(i);
                                }
                                Log.i("info","---------imgs=="+imgs.length);
                                Bundle bundle=new Bundle();
                                bundle.putInt("pos",pos);
                                bundle.putStringArray("img",imgs);
                                Intent intent=new Intent();
                                intent.setClass(mContext, ImagesDisplayActivity.class);
                                intent.putExtras(bundle);
                                mContext.startActivity(intent);
                            }
                        });
                        rowCount++;
                    }

                }
            }

        }else {
            mViewHolder.gridlayout.setVisibility(View.GONE);
        }

        if(list.get(position).getType().equals(mtype)){
            mViewHolder.tv_clear.setVisibility(View.VISIBLE);
        }else {
            mViewHolder.tv_clear.setVisibility(View.GONE);
        }
        if(position==0){
            mViewHolder.tv_note_info.setVisibility(View.VISIBLE);
        }else {
            mViewHolder.tv_note_info.setVisibility(View.GONE);
        }
        if(TextUtils.isEmpty(list.get(position).getRemark())){
            mViewHolder.tv_note.setVisibility(View.GONE);
        }else {
            mViewHolder.tv_note.setVisibility(View.VISIBLE);
            mViewHolder.tv_note.setText(list.get(position).getRemark());
        }
        mViewHolder.tv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.createTwoBtnDialog(mContext, mContext.getString(R.string.determine),
                        mContext.getString(R.string.dialog_confirm),
                        mContext.getString(R.string.dialog_cancel),
                        new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(Dialog dialog) {
                                deleteNote(list.get(position).getRemark_id());
                            }
                        }, new DialogUtils.OnLeftBtnListener() {
                            @Override
                            public void setOnLeftListener(Dialog dialog) {

                            }
                        }, false, true);
            }
        });
        mViewHolder.tv_time.setText(Utils.timedate(list.get(position).getRemark_time()));
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.gridlayout)
        GridLayout gridlayout;
        @BindView(R.id.tv_note)
        TextView tv_note;
        @BindView(R.id.tv_time)
        TextView tv_time;
        @BindView(R.id.tv_clear)
        TextView tv_clear;
        @BindView(R.id.tv_note_info)
        TextView tv_note_info;
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private void deleteNote(String remark_id){
        NetUtil.Companion.getInstance().url(KyyChequeConstants.INSTANCE.getURL_DELETE_NOTE())
                .addParam("remark_id",remark_id)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {

                    }

                    @Override
                    public void onSuccess(@NonNull String s) {

                        EventBus.getDefault().post(new MessageEvent(MessageEvent.CHECK_INFO));
                    }
                },false);
    }
}
