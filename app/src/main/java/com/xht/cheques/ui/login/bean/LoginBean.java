package com.xht.cheques.ui.login.bean;

public class LoginBean {

    /**
     * token_id : 2
     * token : e09c344188a7cac381fc1319971ba63f
     * member_info : {"name":"想想","mobile":"13400134000","sex":"1","avatar":null,"serial_number":"1111"}
     */

    private String token_id;
    private String token;
    private MemberInfoBean member_info;

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public MemberInfoBean getMember_info() {
        return member_info;
    }

    public void setMember_info(MemberInfoBean member_info) {
        this.member_info = member_info;
    }

    public static class MemberInfoBean {
        /**
         * name : 想想
         * mobile : 13400134000
         * sex : 1
         * avatar : null
         * serial_number : 1111
         */

        private String name;
        private String mobile;
        private String sex;
        private Object avatar;
        private String serial_number;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public Object getAvatar() {
            return avatar;
        }

        public void setAvatar(Object avatar) {
            this.avatar = avatar;
        }

        public String getSerial_number() {
            return serial_number;
        }

        public void setSerial_number(String serial_number) {
            this.serial_number = serial_number;
        }
    }
}
