package com.xht.cheques.ui.mine;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.xht.cheque.utils.Login;
import com.xht.cheques.BuildConfig;
import com.xht.cheques.R;
import com.xht.cheques.ui.base.BaseActivity;
import com.xht.cheques.ui.login.LoginActivity;
import com.xht.cheques.ui.mine.adapter.FragmentAdapter;
import com.xht.cheques.ui.mine.bean.MessageEvent;
import com.xht.cheques.ui.mine.bean.MineBean;
import com.xht.cheques.ui.mine.bean.TasknumBean;
import com.xht.cheques.ui.mine.bean.UpdateVersionBean;
import com.xht.cheques.utils.DialogUtils;
import com.xht.cheques.utils.TabLayoutUtils;
import com.xht.kuaiyouyi.api.KyyChequeConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.utils.ToastU;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity {
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.right_view)
    LinearLayout right_view;
    @BindView(R.id.imageview_left)
    ImageView imageview_left;
    @BindView(R.id.textview_title)
    TextView tv_title;
    @BindView(R.id.tl_tabs)
    TabLayout tl_tabs;
    @BindView(R.id.vp_fragment)
    ViewPager viewPager;
    @BindView(R.id.imageview_head)
    ImageView iv_head;
    @BindView(R.id.textView_name)
    TextView tv_name;
    @BindView(R.id.textview_employee)
    TextView tv_employee;
    @BindView(R.id.textView_phone)
    TextView tv_phone;
    @BindView(R.id.bt_exit)
    Button bt_exit;



    public final static int TAB_SET_INDEX = 0;
    public final static int TAB_IN_INDEX = 1;
    public final static int TAB_TAKE_INDEX = 2;
    private int type=TAB_SET_INDEX;
    private String setnum,inmum,oknum;
    private List<Fragment> fragments=new ArrayList<>();
    private String[] tabs;
    private long exitTime;
    private MineBean.TicketCollectorBean ticketCollectorBean;

    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:
                    for (int i = 0; i < tabs.length; i++) {
                        tl_tabs.getTabAt(i).setText(tabs[i]);
                    }
                    break;
                case 2:
                    if(!TextUtils.isEmpty(ticketCollectorBean.getName())){
                        tv_name.setText(ticketCollectorBean.getName());
                    }
                    if(!TextUtils.isEmpty(ticketCollectorBean.getAvatar())){
                        Glide.with(MainActivity.this).load(ticketCollectorBean.getAvatar()).into(iv_head);
                    }
                    if(!TextUtils.isEmpty(ticketCollectorBean.getSerial_number())){
                        tv_employee.setText(ticketCollectorBean.getSerial_number());
                    }
                    if(!TextUtils.isEmpty(ticketCollectorBean.getMobile())){
                        tv_phone.setText(ticketCollectorBean.getMobile());
                    }
                    break;

            }
        }
    };

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        //requestCheckVersion();
        onclick();
        tv_title.setText(getString(R.string.mine_task));
        tabs=new String[]{getString(R.string.take_ticket)+"(0)",getString(R.string.stay_ticket)+"(0)", getString(R.string.have_ticket)+"(0)"};
        imageview_left.setVisibility(View.VISIBLE);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT);
        getInfo();
        showTablist(tabs);
        EventBus.getDefault().register(this);
    }

    private void onclick(){
        imageview_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(right_view);
            }
        });
        bt_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.createTwoBtnDialog(MainActivity.this, getString(R.string.quedinlogin),
                        getString(R.string.dialog_confirm),
                        getString(R.string.dialog_cancel),
                        new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(Dialog dialog) {
                                exitlogin();

                            }
                        }, new DialogUtils.OnLeftBtnListener() {
                            @Override
                            public void setOnLeftListener(Dialog dialog) {

                            }
                        }, false, true);

            }
        });

        right_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    private void exitlogin(){
        NetUtil.Companion.getInstance().url(KyyChequeConstants.INSTANCE.getURL_EXITLOGIN())
                .addParam("ticket_person_id",Login.Companion.getInstance().getToken_id())
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {

                    }

                    @Override
                    public void onSuccess(@NonNull String tasknumBean) {
                        Login.Companion.getInstance().setToken("");
                        Login.Companion.getInstance().setToken_id("");
                        goToActivity(LoginActivity.class);
                        finish();
                    }
                },false);
    }

    private void showTablist(String[] tabs){
        for (int i = 0; i < tabs.length; i++) {
            tl_tabs.addTab(tl_tabs.newTab().setText(tabs[i]));
            TabLayoutUtils.reflex(tl_tabs);
            switch (i) {
                case TAB_SET_INDEX:
                    fragments.add(TaskFragment.newInstance("1"));
                    break;
                case TAB_IN_INDEX:
                    fragments.add(TaskFragment.newInstance("2"));
                    break;
                case TAB_TAKE_INDEX:
                    fragments.add(TaskFragment.newInstance("3"));
                    break;
                default:
                    fragments.add(TaskFragment.newInstance("1"));
                    break;
            }
        }
        viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager(), fragments));
        viewPager.setCurrentItem(TAB_SET_INDEX);//要设置到viewpager.setAdapter后才起作用
        tl_tabs.setupWithViewPager(viewPager);
        tl_tabs.setVerticalScrollbarPosition(TAB_SET_INDEX);
        //tlTabs.setupWithViewPager方法内部会remove所有的tabs，这里重新设置一遍tabs的text，否则tabs的text不显示
        for (int i = 0; i < tabs.length; i++) {
            tl_tabs.getTabAt(i).setText(tabs[i]);
        }
    }

    //任务数量
    private void getTasknum(){
        NetUtil.Companion.getInstance().url(KyyChequeConstants.INSTANCE.getURL_TASK_NUM())
                .withPOST(new NetCallBack<TasknumBean>() {

                    @NotNull
                    @Override
                    public Class<TasknumBean> getRealType() {
                        return TasknumBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(errCode==11009){
                            DialogUtils.createOneBtnDialog(MainActivity.this, "你的账号已在其他设备登录，请重新登录",
                                    "确定", new DialogUtils.OnLeftBtnListener() {
                                        @Override
                                        public void setOnLeftListener(Dialog dialog) {
                                            Login.Companion.getInstance().setToken("");
                                            Login.Companion.getInstance().setToken_id("");
                                            goToActivity(LoginActivity.class);
                                            finish();
                                        }
                                    },false,false);

                        }else {
                            ToastU.INSTANCE.showToast(err);
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull TasknumBean tasknumBean) {
                        if(!TextUtils.isEmpty(tasknumBean.getCount_get()) ){
                            tabs[0]=getResources().getString(R.string.take_ticket)+"("+tasknumBean.getCount_get()+")";
                        }
                        if(!TextUtils.isEmpty(tasknumBean.getCount_in())){
                            tabs[1]=getResources().getString(R.string.stay_ticket)+"("+tasknumBean.getCount_in()+")";
                        }
                        if(!TextUtils.isEmpty(tasknumBean.getCount_already_in())){
                            tabs[2]=getResources().getString(R.string.have_ticket)+"("+tasknumBean.getCount_already_in()+")";
                        }
                        handler.sendEmptyMessage(1);
                    }
                },false);
    }

    //个人信息
    private void getInfo(){
        NetUtil.Companion.getInstance().url(KyyChequeConstants.INSTANCE.getURL_INFO())
                .withPOST(new NetCallBack<MineBean>() {
                    @NotNull
                    @Override
                    public Class<MineBean> getRealType() {
                        return MineBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(errCode==11009){
                            DialogUtils.createOneBtnDialog(MainActivity.this, "你的账号已在其他设备登录，请重新登录",
                                    "确定", new DialogUtils.OnLeftBtnListener() {
                                        @Override
                                        public void setOnLeftListener(Dialog dialog) {
                                            Login.Companion.getInstance().setToken("");
                                            Login.Companion.getInstance().setToken_id("");
                                            goToActivity(LoginActivity.class);
                                            finish();
                                        }
                                    },false,false);

                        }else {
                            ToastU.INSTANCE.showToast(err);
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull MineBean mineBean) {
                        if(mineBean.getTicket_collector()!=null){
                            ticketCollectorBean=mineBean.getTicket_collector();
                            handler.sendEmptyMessage(2);
                        }
                        getTasknum();
                    }
                },false);
    }

    public void exit() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.TASK_NUM)){
            getTasknum();
        }else if(messageEvent.getMessage().equals(MessageEvent.MAIN)){
            MainActivity.this.finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

}
