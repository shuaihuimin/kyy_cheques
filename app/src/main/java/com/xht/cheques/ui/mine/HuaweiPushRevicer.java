/*
 * Copyright (C) Huawei Technologies Co., Ltd. 2016. All rights reserved.
 * See LICENSE.txt for this sample's licensing information.
 */

package com.xht.cheques.ui.mine;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.huawei.hms.support.api.push.PushReceiver;
import com.xht.cheque.utils.Login;


/**
 * 应用需要创建一个子类继承com.huawei.hms.support.api.push.PushReceiver
 */
public class HuaweiPushRevicer extends PushReceiver {


    /**
     *
     * @param context
     * @param tokenIn token值，对应服务端device_token
     * @param extras
     */
    @Override
    public void onToken(Context context, String tokenIn, Bundle extras) {
        Log.i("info","-------hwtokenin"+tokenIn);
        Login.Companion.getInstance().setHuawei_push_token(tokenIn);
        Toast.makeText(context, "token", Toast.LENGTH_SHORT).show();
    }
}
