package com.xht.cheques.ui.mine;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.xht.cheque.utils.Login;
import com.xht.cheques.KyyChequeApp;
import com.xht.cheques.R;
import com.xht.cheques.ui.base.BaseActivity;
import com.xht.cheques.ui.login.LoginActivity;
import com.xht.cheques.ui.mine.adapter.NoteAdapter;
import com.xht.cheques.ui.mine.bean.CheckInfoBean;
import com.xht.cheques.ui.mine.bean.MessageEvent;
import com.xht.cheques.ui.mine.bean.TaskListBean;
import com.xht.cheques.utils.DialogUtils;
import com.xht.cheques.utils.Utils;
import com.xht.cheques.widget.CustomListView;
import com.xht.kuaiyouyi.api.KyyChequeConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.utils.ToastU;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class CheckActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.iv_check)
    ImageView iv_check;
    @BindView(R.id.tv_check_payable)
    TextView tv_check_payable;
    @BindView(R.id.tv_pay_currency)
    TextView tv_pay_currency;
    @BindView(R.id.tv_pay_amount)
    TextView tv_pay_amount;
    @BindView(R.id.tv_trading_currency)
    TextView tv_trading_currency;
    @BindView(R.id.tv_detils)
    TextView tv_detils;
    @BindView(R.id.tv_confirm_ticket)
    TextView tv_confirm_ticket;
    @BindView(R.id.relative_image)
    RelativeLayout relative_image;
    @BindView(R.id.rl_check)
    RelativeLayout rl_check;
    @BindView(R.id.tv_add_note)
    TextView tv_add_note;
    @BindView(R.id.listview)
    CustomListView listView;
    private String type;
    private String payinfo_id;
    private CheckInfoBean.TicketInfoBean mTicketInfoBean;
    private Context context;
    private String url;
    private String flag;
    private List<CheckInfoBean.TicketInfoBean.RemarkArrBean> remarklist;
    private NoteAdapter adapter;
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:
                    if(mTicketInfoBean!=null && !TextUtils.isEmpty(mTicketInfoBean.getTransfer_img())){

                    }else {
                        relative_image.setVisibility(View.GONE);
                    }
                    tv_check_payable.setText(mTicketInfoBean.getHead());
                    tv_pay_currency.setText(mTicketInfoBean.getOrder_main_pay_currency_name());
                    if(!TextUtils.isEmpty(mTicketInfoBean.getActive_status())){
                        if(mTicketInfoBean.getActive_status().equals("1")){
                            tv_pay_amount.setText(mTicketInfoBean.getOrder_main_pay_currency_symbol()
                                    +Utils.getDisplayMoney(Double.parseDouble(mTicketInfoBean.getOrder_main_currency_amount()))
                                    +"（"+getString(R.string.full_payment)+"）");
                        }else if(mTicketInfoBean.getActive_status().equals("2")){
                            tv_pay_amount.setText(mTicketInfoBean.getOrder_main_pay_currency_symbol()
                                    +Utils.getDisplayMoney(Double.parseDouble(mTicketInfoBean.getOrder_main_currency_amount()))
                                    +"（"+getString(R.string.first_phase_payment)+"）");
                        }else if(mTicketInfoBean.getActive_status().equals("3")){
                            tv_pay_amount.setText(mTicketInfoBean.getOrder_main_pay_currency_symbol()
                                    +Utils.getDisplayMoney(Double.parseDouble(mTicketInfoBean.getOrder_main_currency_amount()))
                                    +"（"+getString(R.string.second_phase_payment)+"）");
                        }
                    }
                    tv_trading_currency.setText(mTicketInfoBean.getOrder_main_pay_currency_rate());
                    if(mTicketInfoBean.getTransfer_img()!=null && !TextUtils.isEmpty(mTicketInfoBean.getTransfer_img())){
                        url=mTicketInfoBean.getTransfer_img().substring(mTicketInfoBean.getTransfer_img().length()-3,mTicketInfoBean.getTransfer_img().length());
                        Log.i("info","-----------url"+url);
                        if(url.equals("pdf")){
                            rl_check.setVisibility(View.VISIBLE);
                            iv_check.setVisibility(View.GONE);
                        }else {
                            if(context!=null){
                                Glide.with(context).load(mTicketInfoBean.getTransfer_img()).into(iv_check);
                            }
                        }
                    }
                    break;
            }
        }
    };
    @Override
    protected int getLayout() {
        return R.layout.activity_check;
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        context=getApplicationContext();
        type=getIntent().getStringExtra("type");
        flag=getIntent().getStringExtra("flag");
        remarklist=new ArrayList<>();
        adapter=new NoteAdapter(CheckActivity.this,remarklist,type);
        listView.setAdapter(adapter);

        payinfo_id=getIntent().getExtras().getString("payinfo_id");
        if(type.equals("1")){
            tv_confirm_ticket.setText(getString(R.string.confirm_ticket));
        }else if(type.equals("2")){
            tv_confirm_ticket.setText(getString(R.string.confirm_in_ticket));
        }else {
            tv_confirm_ticket.setVisibility(View.GONE);
            tv_detils.setVisibility(View.GONE);
            tv_add_note.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(payinfo_id)){
            getCheckInfo();
        }
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(KyyChequeApp.activitynum==1){
                    goToActivity(MainActivity.class);
                    finish();
                }else{
                    if(flag.equals("2")){
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.TASK_LIST));
                    }
                    finish();
                }

            }
        });
        tv_confirm_ticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type.equals("1")){
                    showDialog(getString(R.string.confirm_tack));
                }else if(type.equals("2")){
                    showDialog(getString(R.string.confirm_in));
                }
            }
        });
        iv_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTicketInfoBean!=null && !TextUtils.isEmpty(mTicketInfoBean.getTransfer_img())){
                    String imgs[]=new String[1];
                    imgs[0]=mTicketInfoBean.getTransfer_img();
                    Bundle bundle=new Bundle();
                    bundle.putInt("pos",0);
                    bundle.putStringArray("img",imgs);
                    goToActivity(ImagesDisplayActivity.class,bundle);
                }
            }
        });
        rl_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("mUrl",mTicketInfoBean.getTransfer_img());
                goToActivity(PdfActivity.class,bundle);
            }
        });
        tv_add_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("type",type);
                bundle.putString("payinfo_id",payinfo_id);
                goToActivity(UpremarkActivity.class,bundle);
            }
        });

    }

    //支票信息
    private void getCheckInfo(){
        NetUtil.Companion.getInstance()
                .url(KyyChequeConstants.INSTANCE.getURL_CHECK_INFO())
                .addParam("payinfo_id",payinfo_id)
                .withPOST(new NetCallBack<CheckInfoBean>() {

            @NotNull
            @Override
            public Class<CheckInfoBean> getRealType() {
                return CheckInfoBean.class;
            }

            @Override
            public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                if(errCode==11009)
                    DialogUtils.createOneBtnDialog(CheckActivity.this, "你的账号已在其他设备登录，请重新登录",
                            "确定", new DialogUtils.OnLeftBtnListener() {
                                @Override
                                public void setOnLeftListener(Dialog dialog) {
                                    Login.Companion.getInstance().setToken("");
                                    Login.Companion.getInstance().setToken_id("");
                                    Bundle bundle=new Bundle();
                                    bundle.putString("flag","1");
                                    goToActivity(LoginActivity.class,bundle);
                                }
                            },true,true);
                else {
                    ToastU.INSTANCE.showToast(err);
                }
            }

            @Override
            public void onSuccess(@NonNull CheckInfoBean checkInfoBean) {
                mTicketInfoBean=checkInfoBean.getTicket_info();
                remarklist.clear();
                if(checkInfoBean.getTicket_info().getRemark_arr()!=null && checkInfoBean.getTicket_info().getRemark_arr().size()!=0){
                    remarklist.addAll(checkInfoBean.getTicket_info().getRemark_arr());
                    adapter.notifyDataSetChanged();
                    //listView.setAdapter(adapter);
                }
                handler.sendEmptyMessage(1);
            }
        },false);
    }

    //确认取入票
    private void setInCheck(){
        NetUtil.Companion.getInstance()
                .url(KyyChequeConstants.INSTANCE.getURL_IN_CHECK())
                .addParam("payinfo_id",payinfo_id)
                .addParam("type",type)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(errCode==11009)
                            DialogUtils.createOneBtnDialog(CheckActivity.this, "你的账号已在其他设备登录，请重新登录",
                                    "确定", new DialogUtils.OnLeftBtnListener() {
                                        @Override
                                        public void setOnLeftListener(Dialog dialog) {
                                            Login.Companion.getInstance().setToken("");
                                            Login.Companion.getInstance().setToken_id("");
                                            Bundle bundle=new Bundle();
                                            bundle.putString("flag","1");
                                            goToActivity(LoginActivity.class,bundle);
                                        }
                                    },true,true);
                        else {
                            ToastU.INSTANCE.showToast(err);
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull String string) {
                            EventBus.getDefault().post(new MessageEvent(MessageEvent.TASK_LIST));
                            tv_check_payable.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if(type.equals("1")){
                                        ToastU.INSTANCE.showToast(getString(R.string.set_ticket_ok));
                                    }else {
                                        ToastU.INSTANCE.showToast(getString(R.string.in_ticket_ok));
                                    }
                                    //DialogUtils.createTipImageAndTextDialog(CheckActivity.this,getString(R.string.set_ticket_ok),R.mipmap.pop_icon_ok);
                                    if(KyyChequeApp.activitynum==1){
                                        goToActivity(MainActivity.class);
                                    }
                                    CheckActivity.this.finish();
                                }
                            },1000);

                    }
                },false);

    }

    private void showDialog(String content){
        DialogUtils.createTwoBtnDialog(CheckActivity.this, content,
                getString(R.string.dialog_confirm),
                getString(R.string.dialog_cancel),
                new DialogUtils.OnRightBtnListener() {
                    @Override
                    public void setOnRightListener(Dialog dialog) {
                        setInCheck();
                    }
                }, new DialogUtils.OnLeftBtnListener() {
                    @Override
                    public void setOnLeftListener(Dialog dialog) {

                    }
                }, false, true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.CHECK_INFO)){
            getCheckInfo();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

}
