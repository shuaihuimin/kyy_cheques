package com.xht.cheques.ui.mine.bean;

import java.util.List;

public class TaskListBean {


    /**
     * ticket_list : [{"order_main_id":"1235","send_man_time":"1539410133","get_ticket_time":"1539410133","in_ticket_time":"1539410133","order_main_sn":"1000000000139401","address_check_true_name":"123","mobile_zone1":"86","address_check_phone":"13631224521","tel_zone":"","tel_phone":"","address_check_area_info":"澳門 澳門特別行政區","address_check_address":"123123"}]
     * kyy : eyJQIjoxLCJLIjoia3l5XzAwMSJ9
     */

    private String kyy;
    private List<TicketListBean> ticket_list;

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public List<TicketListBean> getTicket_list() {
        return ticket_list;
    }

    public void setTicket_list(List<TicketListBean> ticket_list) {
        this.ticket_list = ticket_list;
    }

    public static class TicketListBean {
        /**
         * order_main_id : 1235
         * send_man_time : 1539410133
         * get_ticket_time : 1539410133
         * in_ticket_time : 1539410133
         * order_main_sn : 1000000000139401
         * address_check_true_name : 123
         * mobile_zone1 : 86
         * address_check_phone : 13631224521
         * tel_zone :
         * tel_phone :
         * address_check_area_info : 澳門 澳門特別行政區
         * address_check_address : 123123
         */

        private String order_main_id;
        private String send_man_time;
        private String get_ticket_time;
        private String in_ticket_time;
        private String order_main_sn;
        private String address_check_true_name;
        private String mobile_zone1;
        private String address_check_phone;
        private String tel_zone;
        private String tel_phone;
        private String address_check_area_info;
        private String address_check_address;
        private String order_payinfo_id;
        private String mobile_zone1_spare;
        private String address_check_phone_spare;
        private String active_status;
        private String mobile_zone2;

        public String getMobile_zone2() {
            return mobile_zone2;
        }

        public void setMobile_zone2(String mobile_zone2) {
            this.mobile_zone2 = mobile_zone2;
        }

        public String getOrder_main_id() {
            return order_main_id;
        }

        public void setOrder_main_id(String order_main_id) {
            this.order_main_id = order_main_id;
        }

        public String getSend_man_time() {
            return send_man_time;
        }

        public void setSend_man_time(String send_man_time) {
            this.send_man_time = send_man_time;
        }

        public String getGet_ticket_time() {
            return get_ticket_time;
        }

        public void setGet_ticket_time(String get_ticket_time) {
            this.get_ticket_time = get_ticket_time;
        }

        public String getIn_ticket_time() {
            return in_ticket_time;
        }

        public void setIn_ticket_time(String in_ticket_time) {
            this.in_ticket_time = in_ticket_time;
        }

        public String getOrder_main_sn() {
            return order_main_sn;
        }

        public void setOrder_main_sn(String order_main_sn) {
            this.order_main_sn = order_main_sn;
        }

        public String getAddress_check_true_name() {
            return address_check_true_name;
        }

        public void setAddress_check_true_name(String address_check_true_name) {
            this.address_check_true_name = address_check_true_name;
        }

        public String getMobile_zone1() {
            return mobile_zone1;
        }

        public void setMobile_zone1(String mobile_zone1) {
            this.mobile_zone1 = mobile_zone1;
        }

        public String getAddress_check_phone() {
            return address_check_phone;
        }

        public void setAddress_check_phone(String address_check_phone) {
            this.address_check_phone = address_check_phone;
        }

        public String getTel_zone() {
            return tel_zone;
        }

        public void setTel_zone(String tel_zone) {
            this.tel_zone = tel_zone;
        }

        public String getTel_phone() {
            return tel_phone;
        }

        public void setTel_phone(String tel_phone) {
            this.tel_phone = tel_phone;
        }

        public String getAddress_check_area_info() {
            return address_check_area_info;
        }

        public void setAddress_check_area_info(String address_check_area_info) {
            this.address_check_area_info = address_check_area_info;
        }

        public String getAddress_check_address() {
            return address_check_address;
        }

        public void setAddress_check_address(String address_check_address) {
            this.address_check_address = address_check_address;
        }

        public String getOrder_payinfo_id() {
            return order_payinfo_id;
        }

        public void setOrder_payinfo_id(String order_payinfo_id) {
            this.order_payinfo_id = order_payinfo_id;
        }

        public String getMobile_zone1_spare() {
            return mobile_zone1_spare;
        }

        public void setMobile_zone1_spare(String mobile_zone1_spare) {
            this.mobile_zone1_spare = mobile_zone1_spare;
        }

        public String getAddress_check_phone_spare() {
            return address_check_phone_spare;
        }

        public void setAddress_check_phone_spare(String address_check_phone_spare) {
            this.address_check_phone_spare = address_check_phone_spare;
        }

        public String getActive_status() {
            return active_status;
        }

        public void setActive_status(String active_status) {
            this.active_status = active_status;
        }
    }
}
