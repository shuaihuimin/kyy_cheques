package com.xht.cheques.ui.mine;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xht.cheques.R;
import com.xht.cheques.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import uk.co.senab.photoview.PhotoView;

public class PreviewActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    private PhotoView topImageView;
    private MyPageAdapter adapter;
    private List<View> listViews = new ArrayList<View>();
    private int position;
    private String imgs[] = null;
    private int location = 1;
    @Override
    protected int getLayout() {
        return R.layout.activity_preview;
    }

    @Override
    protected void initView() {
        position = getIntent().getIntExtra("pos",0);
        imgs = getIntent().getExtras().getStringArray("img");
        tv_num.setText(location+"/"+imgs.length);
        Log.i("info","---------imgll"+imgs.length);
        init();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

        public void onPageSelected(int arg0) {
            location = arg0+1;
            tv_num.setText(location+"/"+imgs.length);
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void init() {
        for (int i = 0; i < imgs.length; i++) {
            View topView = LayoutInflater.from(PreviewActivity.this).inflate(
                    R.layout.activity_larger, null);
            topImageView = (PhotoView) topView
                    .findViewById(R.id.iv_big);
            listViews.add(topImageView);
            Glide.with(PreviewActivity.this).load(imgs[i]).into(topImageView);
            Log.i("info","---------imgs"+imgs[i]);
            topImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        viewPager.setOnPageChangeListener(pageChangeListener);
        adapter = new MyPageAdapter(listViews);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);
    }

    /**
     * 监听返回按钮
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
        }
        return true;
    }


    class MyPageAdapter extends PagerAdapter {

        private List<View> listViews;

        public MyPageAdapter(List<View> listViews) {
            // TODO Auto-generated constructor stub
            this.listViews = listViews;
        }

        @Override
        public int getCount() {
            return imgs.length;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // 防止view里有parent
            ViewGroup parent = (ViewGroup) listViews.get(position).getParent();
            if (parent != null) {
                parent.removeAllViews();
            }
            container.addView(listViews.get(position));
            return listViews.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(listViews.get(position));
        }

    }

}
