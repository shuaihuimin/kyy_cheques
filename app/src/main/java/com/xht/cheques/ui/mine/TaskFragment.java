package com.xht.cheques.ui.mine;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.cheque.utils.Login;
import com.xht.cheques.R;
import com.xht.cheques.ui.base.BaseFragment;
import com.xht.cheques.ui.login.LoginActivity;
import com.xht.cheques.ui.mine.adapter.TaskAdapter;
import com.xht.cheques.ui.mine.bean.MessageEvent;
import com.xht.cheques.ui.mine.bean.TaskListBean;
import com.xht.cheques.ui.mine.bean.TasknumBean;
import com.xht.cheques.utils.DialogUtils;
import com.xht.kuaiyouyi.api.KyyChequeConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.utils.ToastU;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class TaskFragment extends BaseFragment {
    @BindView(R.id.mRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.lv_taskfragment)
    ListView lv_taskfragment;
    @BindView(R.id.tv_refresh_task)
    TextView tv_refresh_task;
    @BindView(R.id.tv_relevant_tasks)
    TextView tv_relevant_tasks;
    private String type;
    private String kyy="";
    private boolean isLodin=true;
    private TaskAdapter adapter;
    private List<TaskListBean.TicketListBean> list=new ArrayList<>();
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:

                    break;
                case 2:
                    adapter.notifyDataSetChanged();
                    if(list.size()==0){
                        tv_relevant_tasks.setVisibility(View.VISIBLE);
                    }else {
                        tv_relevant_tasks.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    };
    public static TaskFragment newInstance(String type) {
        Bundle args = new Bundle();
        args.putString("type",type);
        TaskFragment fragment = new TaskFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        type=getArguments().getString("type");
        Log.i("info","---------type"+type);
        getTaskList(type);
        adapter=new TaskAdapter(getActivity(),list,type);
        lv_taskfragment.setAdapter(adapter);
        initlode();
        initrefresh();
        if(type.equals("1")){
            tv_refresh_task.setVisibility(View.VISIBLE);
        }else {
            RelativeLayout.LayoutParams layoutParams= (RelativeLayout.LayoutParams) smartRefreshLayout.getLayoutParams();
            layoutParams.setMargins(0,0,0,0);
            smartRefreshLayout.setLayoutParams(layoutParams);
        }
        tv_refresh_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kyy="";
                list.clear();
                getTaskList(type);
                EventBus.getDefault().post(new MessageEvent(MessageEvent.TASK_NUM));
            }
        });
        lv_taskfragment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle=new Bundle();
                bundle.putString("flag","1");
                bundle.putString("type",type);
                bundle.putString("payinfo_id",list.get(position).getOrder_payinfo_id());
                goToActivity(CheckActivity.class,bundle);
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.taskfragment_layout;
    }

    private void getTaskList(String type){
        if(isLodin){
            DialogUtils.createTipAllLoadDialog(getActivity(),getString(R.string.loading_text));
        }
        NetUtil.Companion.getInstance().url(KyyChequeConstants.INSTANCE.getURL_TASKLIST())
                .addParam("type",type)
                .withLoadPOST(new NetCallBack<TaskListBean>() {
                    @NotNull
                    @Override
                    public Class<TaskListBean> getRealType() {
                        return TaskListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isLodin){
                            isLodin=false;
                            DialogUtils.moven();
                        }
                        if(errCode==11009)
                            DialogUtils.createOneBtnDialog(getActivity(), "你的账号已在其他设备登录，请重新登录",
                                    "确定", new DialogUtils.OnLeftBtnListener() {
                                        @Override
                                        public void setOnLeftListener(Dialog dialog) {
                                            Login.Companion.getInstance().setToken("");
                                            Login.Companion.getInstance().setToken_id("");
                                            Bundle bundle=new Bundle();
                                            bundle.putString("flag","1");
                                            goToActivity(LoginActivity.class,bundle);
                                            EventBus.getDefault().post(new MessageEvent(MessageEvent.MAIN));
                                        }
                                    },false,false);
                        else {
                            Toast.makeText(getActivity(),getString(R.string.uplode_err),Toast.LENGTH_SHORT).show();
                        }
                        if(smartRefreshLayout.isRefreshing()){
                            smartRefreshLayout.finishRefresh();
                        }
                        if(smartRefreshLayout.isLoading()){
                            smartRefreshLayout.finishLoadmore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull TaskListBean taskListBean) {
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.TASK_NUM));
                        if(isLodin){
                            isLodin=false;
                            DialogUtils.moven();
                        }
                        if(smartRefreshLayout.isRefreshing()){
                            smartRefreshLayout.finishRefresh();
                        }
                        if(smartRefreshLayout.isLoading()){
                            smartRefreshLayout.finishLoadmore();
                        }
                        if(taskListBean.getTicket_list()!=null && taskListBean.getTicket_list().size()!=0){
                            list.addAll(taskListBean.getTicket_list());
                        }else {
                            if(!TextUtils.isEmpty(kyy)){
                                smartRefreshLayout.finishLoadmoreWithNoMoreData();
                            }
                        }
                        kyy=taskListBean.getKyy();
                        handler.sendEmptyMessage(2);
                    }
                },false,kyy);
    }


    private void initlode(){
        smartRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                smartRefreshLayout.resetNoMoreData();
                getTaskList(type);
            }
        });

    }

    private void initrefresh(){
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                smartRefreshLayout.resetNoMoreData();
                kyy="";
                list.clear();
                getTaskList(type);
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.TASK_LIST)){
            kyy="";
            list.clear();
            getTaskList(type);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        DialogUtils.moven();
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

}
