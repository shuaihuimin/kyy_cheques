package com.xht.cheques.ui.mine;

import android.graphics.Canvas;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.xht.cheques.R;
import com.xht.cheques.ui.base.BaseActivity;
import com.xht.cheques.utils.DialogUtils;

import java.io.File;

import butterknife.BindView;
import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.util.FileUtil;

public class PdfActivity extends BaseActivity implements DownloadFile.Listener{
    private ImageView iv_back;
    private RelativeLayout pdf_root;
    private RemotePDFViewPager pdfViewPager;
    private PDFPagerAdapter adapter;
    private String url;
    @Override
    protected int getLayout() {
        return R.layout.activity_pdf;
    }


    @Override
    protected void initView() {
        url=getIntent().getStringExtra("mUrl");
        iv_back=(ImageView) findViewById(R.id.iv_back);
        pdf_root = (RelativeLayout) findViewById(R.id.remote_pdf_root);
        setDownloadListener();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /*设置监听*/
    protected void setDownloadListener() {
        DialogUtils.createTipAllLoadDialog(PdfActivity.this,getString(R.string.loading_text));
        final DownloadFile.Listener listener = this;
        pdfViewPager = new RemotePDFViewPager(this, url, listener);
        pdfViewPager.setId(R.id.pdfViewPager);
    }



    @Override
    public void onSuccess(String url, String destinationPath) {
        DialogUtils.moven();
        adapter = new PDFPagerAdapter(this, FileUtil.extractFileNameFromURL(url));
        pdfViewPager.setAdapter(adapter);
        updateLayout();
    }

    /*更新视图*/
    private void updateLayout() {
        pdf_root.removeAllViewsInLayout();
        pdf_root.addView(pdfViewPager, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onFailure(Exception e) {
        DialogUtils.moven();
    }

    @Override
    public void onProgressUpdate(int progress, int total) {

    }
}
