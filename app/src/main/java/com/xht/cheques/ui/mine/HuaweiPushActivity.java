package com.xht.cheques.ui.mine;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.xht.cheque.utils.Login;
import com.xht.cheques.R;
import com.xht.cheques.ui.base.BaseActivity;
import com.xht.cheques.ui.login.LoginActivity;


/**
 * 华为推送点击通知都是打开这个activity然后再跳转的
 *
 * 自定义动作的内容格式（后端使用到）：
 * intent:#Intent;launchFlags=0x10000000;component=com.xht.kuaiyouyi.debug/com.xht.kuaiyouyi.ui.message.activity.HuaweiPushActivity;S.ext1=helloExt1;S.ext2=helloExt2;end
 */
public class HuaweiPushActivity extends BaseActivity {

    @Override
    protected int getLayout() {
        return R.layout.activity_huaweipush;
    }

    @Override
    protected void initView() {
        getdata(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getdata(intent);
    }

    private void getdata(Intent intent){
        String type = intent.getExtras().getString("type");
        String payinfo_id=intent.getExtras().getString("payinfo_id");
        if(Login.Companion.getInstance().isLogin()){
            if(!TextUtils.isEmpty(payinfo_id)){
                Bundle bundle=new Bundle();
                bundle.putString("flag","2");
                bundle.putString("type","1");
                bundle.putString("payinfo_id",payinfo_id);
                goToActivity(CheckActivity.class,bundle);
            }
        }else {
            goToActivity(LoginActivity.class);
        }
        finish();
    }
}
