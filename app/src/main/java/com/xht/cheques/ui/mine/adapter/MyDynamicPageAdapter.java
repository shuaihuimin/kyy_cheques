package com.xht.cheques.ui.mine.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.xht.cheques.ui.base.BaseActivity;

import java.util.List;

/**
 * 查看动态大图
 */

public class MyDynamicPageAdapter extends PagerAdapter {
    private List<View> listViews;
    private BaseActivity activity;
    private Context mContext;

    public MyDynamicPageAdapter(Context mContext,List<View> listViews) {
        // TODO Auto-generated constructor stub
        this.mContext=mContext;
        activity = (BaseActivity) mContext;
        this.listViews = listViews;
    }

    @Override
    public int getCount() {
        return listViews.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // 防止view里有parent
        ViewGroup parent = (ViewGroup) listViews.get(position).getParent();
        if (parent != null) {
            parent.removeAllViews();
        }

        View view = listViews.get(position);
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                activity.finish();
            }
        });
        container.addView(listViews.get(position));

        return listViews.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(listViews.get(position));
    }

}

