package com.xht.cheques.ui.mine.bean;

import java.util.List;

public class CheckInfoBean {


    /**
     * ticket_info : {"order_main_pay_currency_symbol":"HK$","remark_deleted":1,"head":"新海通电贸商城有限公司","remark_arr":[{"remark_time":"1541464688","remark":"哈哈","remark_id":"4","remark_image_group":["http://192.168.0.2/data/upload/shop/company/"],"order_payinfo_id":"707"},{"remark_time":"1541469894","remark":"哈哈","remark_id":"5","remark_image_group":["http://192.168.0.2/data/upload/shop/company/company_remark_5be0f6c586d15.jpg"],"order_payinfo_id":"707"},{"remark_time":"1541472602","remark_id":"6","remark_image_group":["http://192.168.0.2/data/upload/shop/company/company_remark_5be10156222e9.jpg","http://192.168.0.2/data/upload/shop/company/company_remark_5be10157c7d0f.jpg","http://192.168.0.2/data/upload/shop/company/company_remark_5be101597305f.jpg"],"order_payinfo_id":"707"}],"transfer_img":"http://192.168.0.2/data/upload/shop/company/company_5bc689ac17511.jpg","order_main_id":"1285","order_main_currency_amount":"119.53","order_main_pay_currency_rate":"1:1.1384","active_status":1,"order_main_pay_currency_name":"港币"}
     */

    private TicketInfoBean ticket_info;

    public TicketInfoBean getTicket_info() {
        return ticket_info;
    }

    public void setTicket_info(TicketInfoBean ticket_info) {
        this.ticket_info = ticket_info;
    }

    public static class TicketInfoBean {
        /**
         * order_main_pay_currency_symbol : HK$
         * remark_deleted : 1
         * head : 新海通电贸商城有限公司
         * remark_arr : [{"remark_time":"1541464688","remark":"哈哈","remark_id":"4","remark_image_group":["http://192.168.0.2/data/upload/shop/company/"],"order_payinfo_id":"707"},{"remark_time":"1541469894","remark":"哈哈","remark_id":"5","remark_image_group":["http://192.168.0.2/data/upload/shop/company/company_remark_5be0f6c586d15.jpg"],"order_payinfo_id":"707"},{"remark_time":"1541472602","remark_id":"6","remark_image_group":["http://192.168.0.2/data/upload/shop/company/company_remark_5be10156222e9.jpg","http://192.168.0.2/data/upload/shop/company/company_remark_5be10157c7d0f.jpg","http://192.168.0.2/data/upload/shop/company/company_remark_5be101597305f.jpg"],"order_payinfo_id":"707"}]
         * transfer_img : http://192.168.0.2/data/upload/shop/company/company_5bc689ac17511.jpg
         * order_main_id : 1285
         * order_main_currency_amount : 119.53
         * order_main_pay_currency_rate : 1:1.1384
         * active_status : 1
         * order_main_pay_currency_name : 港币
         */

        private String order_main_pay_currency_symbol;
        private int remark_deleted;
        private String head;
        private String transfer_img;
        private String order_main_id;
        private String order_main_currency_amount;
        private String order_main_pay_currency_rate;
        private String active_status;
        private String order_main_pay_currency_name;
        private List<RemarkArrBean> remark_arr;

        public String getOrder_main_pay_currency_symbol() {
            return order_main_pay_currency_symbol;
        }

        public void setOrder_main_pay_currency_symbol(String order_main_pay_currency_symbol) {
            this.order_main_pay_currency_symbol = order_main_pay_currency_symbol;
        }

        public int getRemark_deleted() {
            return remark_deleted;
        }

        public void setRemark_deleted(int remark_deleted) {
            this.remark_deleted = remark_deleted;
        }

        public String getHead() {
            return head;
        }

        public void setHead(String head) {
            this.head = head;
        }

        public String getTransfer_img() {
            return transfer_img;
        }

        public void setTransfer_img(String transfer_img) {
            this.transfer_img = transfer_img;
        }

        public String getOrder_main_id() {
            return order_main_id;
        }

        public void setOrder_main_id(String order_main_id) {
            this.order_main_id = order_main_id;
        }

        public String getOrder_main_currency_amount() {
            return order_main_currency_amount;
        }

        public void setOrder_main_currency_amount(String order_main_currency_amount) {
            this.order_main_currency_amount = order_main_currency_amount;
        }

        public String getOrder_main_pay_currency_rate() {
            return order_main_pay_currency_rate;
        }

        public void setOrder_main_pay_currency_rate(String order_main_pay_currency_rate) {
            this.order_main_pay_currency_rate = order_main_pay_currency_rate;
        }

        public String  getActive_status() {
            return active_status;
        }

        public void setActive_status(String active_status) {
            this.active_status = active_status;
        }

        public String getOrder_main_pay_currency_name() {
            return order_main_pay_currency_name;
        }

        public void setOrder_main_pay_currency_name(String order_main_pay_currency_name) {
            this.order_main_pay_currency_name = order_main_pay_currency_name;
        }

        public List<RemarkArrBean> getRemark_arr() {
            return remark_arr;
        }

        public void setRemark_arr(List<RemarkArrBean> remark_arr) {
            this.remark_arr = remark_arr;
        }

        public static class RemarkArrBean {
            /**
             * remark_time : 1541464688
             * remark : 哈哈
             * remark_id : 4
             * remark_image_group : ["http://192.168.0.2/data/upload/shop/company/"]
             * order_payinfo_id : 707
             */

            private String remark_time;
            private String remark;
            private String remark_id;
            private String type;
            private String order_payinfo_id;
            private List<String> remark_image_group;

            public String getRemark_time() {
                return remark_time;
            }

            public void setRemark_time(String remark_time) {
                this.remark_time = remark_time;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public String getRemark_id() {
                return remark_id;
            }

            public void setRemark_id(String remark_id) {
                this.remark_id = remark_id;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getOrder_payinfo_id() {
                return order_payinfo_id;
            }

            public void setOrder_payinfo_id(String order_payinfo_id) {
                this.order_payinfo_id = order_payinfo_id;
            }

            public List<String> getRemark_image_group() {
                return remark_image_group;
            }

            public void setRemark_image_group(List<String> remark_image_group) {
                this.remark_image_group = remark_image_group;
            }
        }
    }
}
