package com.xht.cheques.ui;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.huawei.android.hms.agent.HMSAgent;
import com.huawei.android.hms.agent.common.handler.ConnectHandler;
import com.huawei.android.hms.agent.push.handler.GetPushStateHandler;
import com.huawei.android.hms.agent.push.handler.GetTokenHandler;
import com.xht.cheque.utils.Login;
import com.xht.cheques.BuildConfig;
import com.xht.cheques.R;
import com.xht.cheques.ui.base.BaseActivity;
import com.xht.cheques.ui.login.LoginActivity;
import com.xht.cheques.ui.mine.MainActivity;
import com.xht.cheques.ui.mine.bean.UpdateVersionBean;
import com.xht.cheques.utils.DialogUtils;
import com.xht.cheques.utils.Utils;
import com.xht.kuaiyouyi.api.KyyChequeConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class SplashActivity extends BaseActivity {
    @BindView(R.id.imageview_head)
    ImageView imageview_head;
    @Override
    protected int getLayout() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initView() {
        requestCheckVersion();
        if(Utils.isHUAWEI()){
            HMSAgent.connect(this, new ConnectHandler() {
                @Override
                public void onConnect(int rst) {
                    HMSAgent.Push.getToken(new GetTokenHandler() {
                        @Override
                        public void onResult(int rst) {
                            Log.i("info","---------hwtoken"+rst);
                        }
                    });
                    Log.i("info","HMS connect end:" + rst);
                }
            });
        }
    }

    private void requestCheckVersion() {
        NetUtil.Companion.getInstance().url(KyyChequeConstants.INSTANCE.getURL_UPADATE_VERSION())
                .addParam("version", BuildConfig.VERSION_NAME)
                .addParam("client","android")
                .withPOST(new NetCallBack<UpdateVersionBean>() {

                    @NotNull
                    @Override
                    public Class<UpdateVersionBean> getRealType() {
                        return UpdateVersionBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        next();
                    }

                    @Override
                    public void onSuccess(@NonNull final UpdateVersionBean updateVersionBean) {
                        if (updateVersionBean.getRes() != null) {
                            final boolean isForceUpdate = updateVersionBean.getRes().isForceUpdate();
                            if (updateVersionBean.getRes().isIs_update()) {
                                String leftButtonText;
                                if (isForceUpdate) {
                                    leftButtonText = getString(R.string.loginout);
                                } else {
                                    leftButtonText = getString(R.string.dialog_cancel);
                                }
                                DialogUtils.createTwoBtnDialog(SplashActivity.this
                                        , updateVersionBean.getRes().getUpgradePoint()
                                        , getString(R.string.dialog_confirm)
                                        , leftButtonText, new DialogUtils.OnRightBtnListener() {
                                            @Override
                                            public void setOnRightListener(Dialog dialog) {
                                                Uri uri = Uri.parse(updateVersionBean.getRes().getInstallPackage());
                                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                                if (intent.resolveActivity(getPackageManager()) != null) {
                                                    startActivity(intent);
                                                    finish();
                                                }

                                            }
                                        }, new DialogUtils.OnLeftBtnListener() {
                                            @Override
                                            public void setOnLeftListener(Dialog dialog) {
                                                if (isForceUpdate) {
                                                    finish();
                                                } else {
                                                    next();
                                                }
                                            }
                                        }, false, false);
                                return;
                            }
                        }
                        next();
                    }
                }, false);
    }

    public void next() {
        imageview_head.postDelayed(new Runnable() {

            @Override
            public void run() {
                /**
                 * 切换为非全屏
                 */
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                //版本升级，增加了华为推送需要,以前旧版本的用户需要重新登录注册华为推送
                if(Login.Companion.getInstance().getNeed_login_for_register_huaweipush().equals("1")){
                    Login.Companion.getInstance().setToken("");
                    Login.Companion.getInstance().setToken_id("");
                    Login.Companion.getInstance().setNeed_login_for_register_huaweipush("0");
                }
                if (Login.Companion.getInstance().isLogin()) {
                    goToActivity(MainActivity.class);
                } else {
                    goToActivity(LoginActivity.class);
                }
                finish();

            }
        }, 1000);
    }
}
