package com.xht.cheques.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.xht.cheques.R;


/**
 * 底部弹出来的Dialog
 */
public class ButtomDialog extends Dialog {

    private boolean iscancelable;//控制点击dialog外部是否dismiss
    private boolean isBackCancelable;//控制返回键是否dismiss
    private View view;
    private Context context;
    private int height;
    //这里的view其实可以替换直接传layout过来的 因为各种原因没传(lan)
    public ButtomDialog(Context context, View view, boolean isCancelable, boolean isBackCancelable) {
        super(context, R.style.Dialog_translucent);
        this.context = context;
        this.view = view;
        this.iscancelable = isCancelable;
        this.isBackCancelable = isBackCancelable;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(view);//这行一定要写在前面
        setCancelable(iscancelable);//点击外部不可dismiss
        setCanceledOnTouchOutside(isBackCancelable);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        if(height==0){
            params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        }else {
            params.height = height;
        }

        window.setAttributes(params);
        window.setWindowAnimations(R.style.Dialog_buttom_anim);
    }

    public void setHeight(int height){
        this.height = height;
    }
}
