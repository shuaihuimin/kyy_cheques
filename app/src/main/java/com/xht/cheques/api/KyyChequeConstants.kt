package com.xht.kuaiyouyi.api

import com.xht.cheques.BuildConfig


/**
 * 常量类
 *
 * @author 慧敏
 * @Time 2017-12-28
 * @Email 俊华
 */
object KyyChequeConstants {

    /**
     * 服务器域名
     */
    val HOST = BuildConfig.DOMAIN

    /**
     * 应用上下文完整路径
     */
    val URL_CONTEXTPATH = HOST+"mobile/index.php?"
    /**
     * 登录
     */
    val URL_LOGIN= URL_CONTEXTPATH+"act=xht_ticket&op=get_token"
    /**
     * 退出登录
     */
    val URL_EXITLOGIN= URL_CONTEXTPATH+"act=xht_ticket&op=quit_login"
    /**
     * 我的任务数量
     */
    val URL_TASK_NUM= URL_CONTEXTPATH+"act=xht_ticket&op=ticket_count"
    /**
     * 个人信息
     */
    val URL_INFO= URL_CONTEXTPATH+"act=xht_ticket&op=staff_message"
    /**
     * 任务列表
     */
    val URL_TASKLIST= URL_CONTEXTPATH+"act=xht_ticket&op=task"
    /**
     * 分页
     */
    val URL_TASKLISTNIET= URL_CONTEXTPATH+"act=xht_ticket_kyy&op=index"
    /**
     * 支票信息
     */
    val URL_CHECK_INFO= URL_CONTEXTPATH+"act=xht_ticket&op=ticket_message"
    /**
     * 确认入取票
     */
    val URL_IN_CHECK= URL_CONTEXTPATH+"act=xht_ticket&op=get_ticket"
    /**
     * 版本更新
     */
    val URL_UPADATE_VERSION= URL_CONTEXTPATH+"act=xht_ticket&op=versionUpdate"

    /**
     * 上传备注图片
     */
    val URL_UPLODE_IMG= URL_CONTEXTPATH+"act=xht_ticket&op=upload"
    /**
     * 上传备注
     */
    val URL_UPLODE_NOTE= URL_CONTEXTPATH+"act=xht_ticket&op=remark_add"
    /**
     * 上传备注多图片
     */
    val URL_UPLODE_IMGMORE= URL_CONTEXTPATH+"act=xht_ticket&op=upload_multi"
    /**
     * 删除备注
     */
    val URL_DELETE_NOTE= URL_CONTEXTPATH+"act=xht_ticket&op=remark_delete"
}
