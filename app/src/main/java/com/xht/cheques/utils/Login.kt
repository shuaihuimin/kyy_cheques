package com.xht.cheque.utils

/**
 * Created by shuaihuimin on 2018/6/15.
 */

import android.text.TextUtils
import android.util.Log
import com.xht.cheques.utils.Preference

/**
 * Created by pjh on 18/3/8.
 * 登录管理
 */
class Login {

    private constructor()

    object Inner {
        val ins: Login = Login()
    }

    companion object {
        fun getInstance(): Login {
            return Inner.ins
        }
    }

    //    {"username":"test123456","key":"f36626b0f95b557471fa989d0674c009","userid":"31"}


    var username: String by Preference("login_username", "")
    var token: String by Preference("login_token", "")
    var token_id: String by Preference("login_token_id", "")
    var phone: String by Preference("phone","")

    var huawei_push_token: String by Preference("huawei_push_token", "")
    //需要重新登录注册华为的推送
    var need_login_for_register_huaweipush: String by Preference("need_login_for_register_huaweipush", "1")




    fun isLogin(): Boolean {
        Log.i("info","----------token_id"+token_id);
        if (TextUtils.isEmpty(token)) {
            return false
        } else if (TextUtils.isEmpty(token_id)) {
            return false
        } else {
            return true
        }

    }

}