package com.xht.cheque.utils


import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Environment
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import com.xht.cheques.KyyChequeApp
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by 俊华 on 2018/1/15 0015.
 * 通用系统工具
 */
object SystemUtil {

    private val TAG="SystemUtil"
    /*
     * bitmap转file(原图转换)
     */
    /**
     * 把batmap 转file
     * @param bitmap
     * @param filepath
     */
    fun saveBitmapFile(bitmap: Bitmap, filepath: String,fileName:String): File {
        val file = File(filepath)//将要保存图片的路径
        if (!file.exists()){
            file.mkdirs()
        }
        val myCaptureFile = File(filepath + fileName)
        Log.e(TAG,"file:  -> "+file.absolutePath)
        try {
            val bos = BufferedOutputStream(FileOutputStream(myCaptureFile))
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
            bos.flush()
            bos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return myCaptureFile
    }

    //判断sdcard是否被挂载
    fun hasSdcard(): Boolean {
        //判断ＳＤ卡手否是安装好的　　　media_mounted
        return if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            true
        } else {
            false
        }
    }

    fun dp2px(context: Context, dp: Float): Int {
        //获取设备密度
        var density = context.getResources().getDisplayMetrics().density
        //4.3, 4.9, 加0.5是为了四舍五入
        var px = (dp * density + 0.5f).toInt()
        return px
    }

    fun px2dp(context: Context, px: Int): Float {
        //获取设备密度
        var density = context.getResources().getDisplayMetrics().density
        var dp = px / density
        return dp
    }


    /**
     * 检查是否安装微信客户端
     */
    fun isWeixinAvilible(): Boolean {
        var packageManager: PackageManager = KyyChequeApp.context.getPackageManager();// 获取packagemanager
        var pinfo = packageManager.getInstalledPackages(0)// 获取所有已安装程序的包信息
        if (pinfo != null) {

            for (i in pinfo) {
                if (i.packageName.equals("com.tencent.mm")) {
                    return true
                }
            }

        }
        return false
    }


    // 将字符串转为时间戳
    fun getTime(user_time: String): String? {
        var re_time: String? = null
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val d: Date

        try {
            d = sdf.parse(user_time)
            val l = d.time
            val str = l.toString()
            re_time = str.substring(0, 10)
        } catch (e: java.text.ParseException) {
            e.printStackTrace()
        }

        return re_time
    }

    // 将时间戳 转为字符串
    fun getTimeStr(user_time: String = "${System.currentTimeMillis()}"): String {


        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        if (user_time.length == 10) {
            val date = Date(user_time.toLong() * 1000)
            return simpleDateFormat.format(date)
        } else if (user_time.length == 13) {
            val date = Date(user_time.toLong())
            return simpleDateFormat.format(date)
        }


        return ""
    }

    // 将时间戳 转为字符串
    fun getDayStr(user_time: String = "${System.currentTimeMillis()}"): String {


        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        if (user_time.length == 10) {
            val date = Date(user_time.toLong() * 1000)
            return simpleDateFormat.format(date)
        } else if (user_time.length == 13) {
            val date = Date(user_time.toLong())
            return simpleDateFormat.format(date)
        }


        return ""
    }

    // 将时间戳 转为字符串
    fun getTimeStrShort(user_time: Long): String {
        val res: String
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date = Date(user_time)
        res = simpleDateFormat.format(date)
        return res
    }


    // 将字符串转为时间戳
    fun getTimeLong(user_time: String): Long {
        var re_time: Long = 0
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val d: Date

        try {
            d = sdf.parse(user_time)
            re_time = d.time
        } catch (e: java.text.ParseException) {
            e.printStackTrace()
        }

        return re_time
    }

    /**
     * 获取当前机器的屏幕信息对象<br></br>
     * 另外：通过android.os.Build类可以获取当前系统的相关信息
     *
     * @param context
     * @return
     */
    fun getScreenInfo(context: Context): DisplayMetrics {
        val windowManager = context
                .getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val dm = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(dm)
        // dm.widthPixels;//寬度
        // dm.heightPixels; //高度
        // dm.density; //密度
        return dm
    }


//    //生成二维码
//    fun createQRCode(str: String, widthAndHeight: Int): Bitmap {
//        var hints = Hashtable<EncodeHintType, String>()
//        hints.put(EncodeHintType.CHARACTER_SET, "utf-8")
//        var matrix: BitMatrix = MultiFormatWriter().encode(str,
//                BarcodeFormat.QR_CODE, widthAndHeight + 100, widthAndHeight + 100)
//        var width = matrix.getWidth()
//        var height = matrix.getHeight()
//        var pixels: IntArray = IntArray(width * height)
//
//        Log.e(TAG,"数组大小 --->  " + pixels.size)
//        /*       for (int y = 0; y < height; y++) {
//                   for (int x = 0; x < width; x++) {
//                   if (matrix.get(x, y)) {
//                       pixels[y * width + x] = BLACK;
//                   }
//               }
//               }*/
//
//        for (y in 0 until height) {
//            for (x in 0 until width) {
//                if (matrix.get(x, y)) {
//                    pixels[y * width + x] = BLACK
//                }
//            }
//        }
//
//        var bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
//        bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
//        return bitmap
//    }


    /**
     * 获取所有方法
     */
    fun printAllInform(clsShow: Class<*>) {
        try {
            // 取得所有方法
            val hideMethod = clsShow.methods
            var i = 0
            while (i < hideMethod.size) {
                Log.e(TAG,"method name : "+hideMethod[i].name + ";and the i is:"
                        + i)
                i++
            }
            // 取得所有常量
            val allFields = clsShow.fields
            i = 0
            while (i < allFields.size) {
                Log.e(TAG,"Field name : "+allFields[i].name)
                i++
            }
        } catch (e: SecurityException) {
            // throw new RuntimeException(e.getMessage());
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            // throw new RuntimeException(e.getMessage());
            e.printStackTrace()
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

    }



    /**
     * 截取字符串
     * @param str 待截取的字符串
     * @param start 截取起始位置 （ 1 表示第一位 -1表示倒数第1位）
     * @param end 截取结束位置 （如上index）
     * @return
     */
    fun subString(str: String?, start: Int, end: Int): String {
        var start = start
        var end = end
        val result: String? = null

        if (str == null || str == "")
            return ""

        val len = str.length
        start = if (start < 0) len + start else start - 1
        end = if (end < 0) len + end + 1 else end

        return str.substring(start, end)
    }


}